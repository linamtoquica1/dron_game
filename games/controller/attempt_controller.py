from games.controller.round_controller import RoundController
from games.models import Attempt


class AttemptController:
    """
    Controller to execute logic for game attempts
    """

    def __init__(self, game):
        self.game = game
        self.round_number = self.set_round_number()

    def set_round_number(self):
        round_controller = RoundController(self.game)
        round_number = round_controller.get_next_round()
        return round_number

    def get_attempt(self):
        attempt_game = Attempt.objects.filter(round_number=self.round_number)
        print(">>", attempt_game)
        if attempt_game and attempt_game.last().attempt_number:
                attempt_game = attempt_game.last().attempt_number + 1

        else:
            attempt_game = 1

        return attempt_game

    def set_winner_attempt(self, player1_move, player2_move):
        print("round", self.round_number)
        print("intentos:", self.get_attempt())

        if self.round_number:
            attempt_game = Attempt.objects.create(
                round_number=self.round_number,
                attempt_number=self.get_attempt(),
                player_1_move=player1_move,
                player_2_move=player2_move,
                winner=self._find_winner_by_trial(player1_move, player2_move)
            )

            return attempt_game

    def _find_winner_by_trial(self, player1_move, player2_move):
        player_number = 0
        for answer_combination in POSIBLE_ANSWERS:
            if answer_combination[0] == (player1_move, player2_move):
                player_number = answer_combination[1]

        if player_number == 1:
            return self.game.player_1
        elif player_number == 2:
            return self.game.player_2

    def count_won_attemts(self, player):
        won_attempts_player = Attempt.objects.filter(round_number=self.round_number, winner=player)
        return len(won_attempts_player)


POSIBLE_ANSWERS = (
    (("rock", "paper"), 2),
    (("paper", "rock"), 1),
    (("rock", "scissors"), 1),
    (("scissors", "rock"), 2),
    (("paper", "scissors"), 2),
    (("scissors", "paper"), 1),

)
