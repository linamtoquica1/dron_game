from games.models import Round


class RoundController:
    def __init__(self, game):
        self.game = game

    def get_last_round_number(self):
        round_game = Round.objects.filter(game=self.game, game__winner__isnull=False)
        if round_game:
            if round_game.last().round_number < 3:
                round_number = round_game.last().round_number + 1
            else:
                round_number = None
        else:
            round_number = 1

        return round_number

    def get_next_round(self):
        round_game = Round.objects.filter(game=self.game).last()
        if round_game is None:
            round_game = Round.objects.create(
                game=self.game,
                round_number=1
            )
        else:

            if round_game.winner:
                round_game = Round.objects.create(
                    game=self.game,
                    round_number=round_game.round_number + 1
                )

        print(round_game.round_number, round_game.winner)
        return round_game

    def get_actual_round(self):
        round_game = Round.objects.filter(game=self.game).last()
        return round_game.round_number

    def set_winner_round(self, winner):
        round_game = Round.objects.filter(game=self.game).last()
        round_game.winner = winner
        round_game.save()

    def count_won_rounds(self, player):
        won_round_player = Round.objects.filter(game=self.game, winner=player)
        return len(won_round_player)






