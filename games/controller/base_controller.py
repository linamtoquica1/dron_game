# from games.models import Round
from games.controller.attempt_controller import AttemptController
from games.controller.round_controller import RoundController
from players.controller.player_controller import PlayerController


class GameController:

    def __init__(self, game):
        self.game = game
        self.player1 = self.game.player_1
        self.player2 = self.game.player_2
        self.attempt_controller = None
        self.round_controller = None

    def play(self, player1_move, player2_move):
        self.attempt_controller = AttemptController(self.game)
        self.round_controller = RoundController(self.game)

        attempt_reponse = self.attempt_controller.set_winner_attempt(player1_move, player2_move)
        print("res:", attempt_reponse)
        if self.game.winner is None:
            winner_round = self.validate_someone_won_round()
            if winner_round is None:
                message = "continue playing"
            else:
                self.round_controller.set_winner_round(winner_round)

                print("player:", winner_round, "has won round")
                winner_game = self.validate_someone_won_game()
                if winner_game is None:
                    print("continue playing next round")
                    message = f"player: {winner_round} has won round. Continue playing next round"
                else:
                    self.set_winner_game(winner_game)
                    message = f"player: {winner_game} has won game"
                    print("player: ", winner_game, "has won game")
                    player_controller = PlayerController(winner_game)
                    player_controller.add_score_player()
        else:
            message = "Someone has won this game yet."
            print("Someone has won this game.")

        response = {
            "round": self.round_controller.get_actual_round(),
            "player_1_score": self.attempt_controller.count_won_attemts(self.player1),
            "player_2_score": self.attempt_controller.count_won_attemts(self.player2),
            "message": message
        }
        return response

    def validate_someone_won_round(self):
        if self.attempt_controller.count_won_attemts(self.player1) >= 3:
            return self.player1
        elif self.attempt_controller.count_won_attemts(self.player2) >= 3:
            return self.player2

    def validate_someone_won_game(self):
        if self.round_controller.count_won_rounds(self.player1) >= 3:
            return self.player1
        elif self.round_controller.count_won_rounds(self.player2) >= 3:
            return self.player2

    def set_winner_game(self, player):
        self.game.winner = player
        self.game.save()










