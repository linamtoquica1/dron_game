from rest_framework import serializers

from games.models import Game, Round
from players.models import Player
from players.serializers import PlayerSerializer


class GameSerializer(serializers.ModelSerializer):
    """
    GameSerializer:
    create a new game and save the players
    """
    player_1 = PlayerSerializer()
    player_2 = PlayerSerializer()

    class Meta:
        model = Game
        fields = ('id', 'player_1', 'player_2', 'winner')


    def create(self, validated_data):
        player1, created = Player.objects.get_or_create(
            name=validated_data['player_1']['name']
        )
        player2, created = Player.objects.get_or_create(
            name=validated_data['player_2']['name']
        )

        game = Game.objects.create(
            player_1=player1,
            player_2=player2
        )

        return game


class RoundSerializer(serializers.ModelSerializer):
    """
    Create, update a round for a Game
    """

    class Meta:
        model = Round
        fields = ('id', 'game', 'winner')


class AttemptSerializer(serializers.ModelSerializer):
    """
        Create, update a round for a Game
        """
    class Meta:
        model = Round
        fields = ('id', 'round_number', 'player_1_move', 'player_2_move', 'attempt_number', 'winner'),



