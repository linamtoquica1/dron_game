# Generated by Django 2.0 on 2018-11-17 21:06

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('players', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Game',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_game', models.DateTimeField(auto_now=True)),
                ('player_1', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, related_name='player1', to='players.Player')),
                ('player_2', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, related_name='player2', to='players.Player')),
                ('winner', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='winner_game', to='players.Player')),
            ],
        ),
        migrations.CreateModel(
            name='Round',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('player_1_move', models.CharField(blank=True, max_length=50, null=True)),
                ('player_2_move', models.CharField(blank=True, max_length=50, null=True)),
                ('round_number', models.IntegerField()),
                ('game', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, to='games.Game')),
                ('winner', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.DO_NOTHING, related_name='winner', to='players.Player')),
            ],
        ),
    ]
