from django.db import models

from games.choices import MOVES


class Game(models.Model):
    """
    Game:
    Information about games played
    """
    player_1 = models.ForeignKey('players.Player', on_delete=models.DO_NOTHING, related_name='player1')
    player_2 = models.ForeignKey('players.Player', on_delete=models.DO_NOTHING, related_name='player2')
    winner = models.ForeignKey('players.Player', on_delete=models.DO_NOTHING, related_name='winner_game',
                               null=True, blank=True)
    date_game = models.DateTimeField(auto_now=True)

    def __str__(self):
        return f"{self.player_1} Vs. {self.player_2}"


class Round(models.Model):
    """
    Round:
    Information about number of rounds to one games.
    """
    game = models.ForeignKey('Game', on_delete=models.DO_NOTHING)
    round_number = models.IntegerField(null=True, blank=True)
    winner = models.ForeignKey('players.Player', on_delete=models.DO_NOTHING, related_name='winner_round',
                               null=True, blank=True)

    def __str__(self):
        return str(self.round_number)


class Attempt(models.Model):
    round_number = models.ForeignKey('Round', on_delete=models.DO_NOTHING)
    player_1_move = models.CharField(max_length=50, null=True, blank=True)
    player_2_move = models.CharField(max_length=50, null=True, blank=True)
    attempt_number = models.IntegerField(null=True, blank=True)
    winner = models.ForeignKey('players.Player', on_delete=models.DO_NOTHING, related_name='winner_attempt',
                               null=True, blank=True)
