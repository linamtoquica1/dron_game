import logging
from rest_framework import viewsets, status
from rest_framework.response import Response
from rest_framework.views import APIView

from games.controller.base_controller import GameController
from games.models import Game, Round
from games.serializers import GameSerializer, RoundSerializer

logger = logging.getLogger('raven')


class GameViewSet(viewsets.ModelViewSet):
    """
    A simple ViewSet for viewing and editing games.
    """
    queryset = Game.objects.all()
    serializer_class = GameSerializer


class RoundViewSet(viewsets.ViewSet):
    """
    A simple ViewSet for listing or retrieving users.
    """
    def list(self, request):
        queryset = Round.objects.all()
        serializer = RoundSerializer(queryset, many=True)

        logger.error('There is a call to api list rounds', exc_info=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        queryset = Round.objects.filter(game_id=pk)

        serializer = RoundSerializer(queryset, many=True)
        return Response(serializer.data)

    def create(self, request):
        serializer = RoundSerializer(data=request.data)

        if not serializer.is_valid():
            return Response(serializer.errors,
                            status=status.HTTP_400_BAD_REQUEST)
        serializer.save()
        return Response(serializer.data)


class PlayGame(APIView):
    """
    Play game with two moves

    """

    def post(self, request, format=None):
        data = request.data
        game = Game.objects.get(id=data.get("game"))
        game_controller = GameController(game)

        try:
            response_game = game_controller.play(data.get("player1_move"), data.get("player2_move"))

            return Response(response_game, status=status.HTTP_201_CREATED)
        except Exception as e:
            error = {
                "error": e
            }
            logger.error(f'PlayGame: There is an error : {e}', exc_info=True)
            return Response(error, status=status.HTTP_400_BAD_REQUEST)
