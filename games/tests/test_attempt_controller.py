from pytest import mark

from games.controller.attempt_controller import AttemptController
from games.tests.factories import GameFactory, RoundFactory, AttemptFactory
from players.tests.factories import PlayerFactory




@mark.django_db
def test_find_winner_by_trial_player_2():
    player1 = PlayerFactory.create(name="lina")
    player2 = PlayerFactory.create(name="maria")
    game = GameFactory.create(player_1=player1, player_2=player2)

    attempt_controller = AttemptController(game)
    winner = attempt_controller._find_winner_by_trial("paper", "scissors")

    assert winner == player2


@mark.django_db
def test_find_winner_by_trial_player_1():
    player1 = PlayerFactory.create(name="lina")
    player2 = PlayerFactory.create(name="maria")
    game = GameFactory.create(player_1=player1, player_2=player2)

    attempt_controller = AttemptController(game)
    user = attempt_controller._find_winner_by_trial("paper", "rock")

    assert user == player1


@mark.django_db
def test_find_winner_by_trial_equal():
    player1 = PlayerFactory.create(name="lina")
    player2 = PlayerFactory.create(name="maria")
    game = GameFactory.create(player_1=player1, player_2=player2)

    attempt_controller = AttemptController(game)
    user = attempt_controller._find_winner_by_trial("paper", "paper")

    assert user is None


@mark.django_db
def test_set_round_number():
    player1 = PlayerFactory.create(name="lina")
    player2 = PlayerFactory.create(name="maria")
    game = GameFactory.create(player_1=player1, player_2=player2)
    round = RoundFactory.create(game=game)
    attempt = AttemptFactory.create(round_number=round)
    print(attempt.round_number)

    attempt_controller = AttemptController(game)

    result = attempt_controller.set_round_number()

    assert result.round_number == 1


@mark.django_db
def test_set_winner_attempt():
    player1 = PlayerFactory.create(name="lina")
    player2 = PlayerFactory.create(name="maria")
    game = GameFactory.create(player_1=player1, player_2=player2)
    round = RoundFactory.create(game=game)
    attempt = AttemptFactory.create(round_number=round)
    attempt_controller = AttemptController(game)

    result = attempt_controller.set_winner_attempt("scissors", "paper")

    assert result.attempt_number == 1
    assert result.winner == player1


@mark.django_db
def test_get_attempt():
    player1 = PlayerFactory.create(name="lina")
    player2 = PlayerFactory.create(name="maria")
    game = GameFactory.create(player_1=player1, player_2=player2)
    round = RoundFactory.create(game=game)
    attempt = AttemptFactory.create(round_number=round)
    attempt_controller = AttemptController(game)

    result = attempt_controller.get_attempt()

    assert result == 1

