import factory
import pytest

from games.models import Game, Round, Attempt


@pytest.mark.django_db
class GameFactory(factory.django.DjangoModelFactory):

    class Meta:
        model = Game


@pytest.mark.django_db
class RoundFactory(factory.django.DjangoModelFactory):
    round_number = 1

    class Meta:
        model = Round


@pytest.mark.django_db
class AttemptFactory(factory.django.DjangoModelFactory):

    class Meta:
        model = Attempt
