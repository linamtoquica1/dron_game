from django.conf.urls import url
from django.urls import include
from rest_framework.routers import DefaultRouter

from games import views
from games.views import PlayGame

router = DefaultRouter()
router.register(r'games', views.GameViewSet)
router.register(r'rounds', views.RoundViewSet, base_name="rounds")


urlpatterns = [
    url(r'^', include(router.urls)),
    url(r'^play/$', PlayGame.as_view(), name='play_game'),
]
