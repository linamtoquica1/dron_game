#!/usr/bin/env bash
export $(cat .env | sed -e /^$/d -e /^#/d | xargs)
python3 manage.py runserver 0.0.0.0:8000
