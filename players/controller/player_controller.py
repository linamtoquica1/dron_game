

class PlayerController:

    def __init__(self, player):
        self.player = player

    def add_score_player(self):
        self.player.won_games = self.player.won_games + 1 if self.player.won_games else 1
        self.player.save()
