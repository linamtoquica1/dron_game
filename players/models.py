from django.db import models


class Player(models.Model):
    """
    Player:
    Information about players
    """
    name = models.CharField(max_length=100)
    won_games = models.IntegerField(null=True, blank=True, default=0)

    def __str__(self):
        return self.name
