import factory
from pytest import mark

from players.models import Player

@mark.dgango_db
class PlayerFactory(factory.django.DjangoModelFactory):

    class Meta:
        model = Player
