from django.core.exceptions import ObjectDoesNotExist
from rest_framework import viewsets, status
from rest_framework.generics import get_object_or_404

from players.models import Player
from players.serializers import PlayerSerializer
from rest_framework.response import Response


# class PlayerViewSet(viewsets.ModelViewSet):
#     """
#
#     """
#     queryset = Player.objects.all()
#     serializer_class = PlayerSerializer


class PlayerViewSet(viewsets.ViewSet):
    """
    A simple ViewSet for listing or retrieving users.
    """
    def list(self, request):
        queryset = Player.objects.all()
        serializer = PlayerSerializer(queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        queryset = Player.objects.all()
        player = get_object_or_404(queryset, pk=pk)
        serializer = PlayerSerializer(player)
        return Response(serializer.data)

    def create(self, request):
        serializer = PlayerSerializer(data=request.data)

        if not serializer.is_valid():
            return Response(serializer.errors,
                            status=status.HTTP_400_BAD_REQUEST)
        serializer.save()
        return Response(serializer.data)



