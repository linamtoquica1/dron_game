from django.conf.urls import url
from django.urls import include
from rest_framework.routers import DefaultRouter

from players import views

router = DefaultRouter()
router.register(r'players', views.PlayerViewSet, basename='Players')


urlpatterns = [
            url(r'^', include(router.urls)),

]
