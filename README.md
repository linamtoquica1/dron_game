# dron_game

This is a Paper, rock, scissors game bakend in Django Framework

### How do I get set up? ###

* you have to put in proyect root a file named .env (attachment in email)
* run: docker-compose build
* run: docker-compose up

### for testing ###
* run: docker exec -it dron_game_web_1  bash
* run: pytest -vvvv
